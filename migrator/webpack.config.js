const NodemonPlugin = require("nodemon-webpack-plugin");
const path = require("path");
const webpack = require("webpack");
const { NormalModuleReplacementPlugin } = require("webpack");

module.exports = {
  mode: "development",
  entry: "./src/main.ts",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "app.js",
  },
  stats: "minimal",
  watchOptions: {
    ignored: "./dist",
  },
  target: "node",
  resolve: {
    extensions: [".js", ".json", ".ts"],
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: "ts-loader",
      },
    ],
  },
  devtool: "source-map",
  plugins: [
    new NodemonPlugin({ quiet: true, nodeArgs: ["--inspect"] }),
    new webpack.WatchIgnorePlugin({
      paths: ["./dist"],
    }),
    // Ignore knex dynamic required dialects that we don't use
    new NormalModuleReplacementPlugin(
      /mongodb-client-encryption|aws4|socks|snappy|gcp-metadata|kerberos|\@mongodb-js\/zstd|\@aws-sdk\/credential-providers/,
      "noop2"
    ),
  ],
};
