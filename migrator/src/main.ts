import amqp from "amqplib";
import { MongoClient } from "mongodb";
import express from "express";
import logger from "./utils/logger";

import { RabbitMQPropagator } from "./propagator/rabbitPropagator";
import { InitializationService } from "./initialization/initializationService";
import { CoordinationService } from "./coordination/coordinationService";

import { useInitializationRabbitMQController } from "./initialization/controllers/rabbitMQ";
import { useCoordinatorRabbitMQController } from "./coordination/controllers/coordination";

import { MongoStateRepository } from "./persistance/state/mongoMemoryStateRepository";
import { MockReportRepository } from "./persistance/reports/mockReportRepository";

import {
  RabbitMQConfiguration,
  MongoDBConfiguration,
  ExpressConfiguration,
} from "./config";
import { createReportController } from "./reports/expressController";
import { MongoReportsRepository } from "./persistance/reports/mongoReportRepository";

(async () => {
  let rabbitConnection;
  try {
    rabbitConnection = await amqp.connect(RabbitMQConfiguration);
    logger.info(`Connected to RabbitMQ !`);
  } catch (err) {
    logger.error(`Cannot connect to rabbitMQ: ${err} `);
    process.exit(1);
  }

  let collection;
  try {
    const { address, database, port, user, password } = MongoDBConfiguration;
    const mongoClient = await MongoClient.connect(
      `mongodb://${user}:${password}@${address}:${port}`
    );
    const db = mongoClient.db(database);
    collection = db.collection("state");
    logger.info(`Connected to MongoDB !`);
  } catch (err) {
    logger.error(`Cannot connect to mongoDB: ${err} `);
    process.exit(1);
  }

  const stateRepository = new MongoStateRepository(collection);
  const propagator = new RabbitMQPropagator(rabbitConnection);

  const initializationService = new InitializationService(
    propagator,
    stateRepository
  );
  const coordinationService = new CoordinationService(
    propagator,
    stateRepository
  );

  const app = express();
  app.use(express.json());

  if (ExpressConfiguration.mock) {
    app.use("/reports", createReportController(new MockReportRepository()));
  } else {
    app.use(
      "/reports",
      createReportController(new MongoReportsRepository(collection))
    );
  }

  app.listen(ExpressConfiguration.port, () =>
    logger.info(`Express listen on port ${ExpressConfiguration.port}`)
  );

  useInitializationRabbitMQController(rabbitConnection, initializationService);
  useCoordinatorRabbitMQController(rabbitConnection, coordinationService);
})();
