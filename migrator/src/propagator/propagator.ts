import { AlgorithmConfiguration } from "../types/configuration";

export interface WorkPortion extends Omit<AlgorithmConfiguration, "topology"> {
  nodeId: string;
  population?: any[];
}

export interface Propagator {
  sendWorkPortion: (destination: string, workPortion: WorkPortion) => void;
}
