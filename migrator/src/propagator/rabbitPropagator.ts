import { Channel, Connection } from "amqplib";
import { Propagator, WorkPortion } from "./propagator";
import logger from "../utils/logger";

export class RabbitMQPropagator implements Propagator {
  private channel: Channel | null = null;
  constructor(connection: Connection) {
    connection.createChannel().then((channel) => (this.channel = channel));
  }

  sendWorkPortion = (destination: string, workPortion: WorkPortion) => {
    if (!this.channel) {
      logger.error("Cannot send work portion. Not connected");
      return;
    }

    const body = {
      algorithm: workPortion.algorithm,
      problem: workPortion.problem,
      population: workPortion.population,
    };

    const { id, nodeId } = workPortion;
    const headers = {
      id,
      nodeId,
      path: ["migrator.work"],
    };
    logger.info(
      `Send work portion (id: ${id}, nodeId: ${nodeId}) to ${destination}`
    );
    this.channel.sendToQueue(destination, Buffer.from(JSON.stringify(body)), {
      headers,
    });
  };
}
