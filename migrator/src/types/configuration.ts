export interface AlgorithmConfiguration {
  id: string;
  problem: any;
  algorithm: any;
  topology: Topology;
}

export interface Topology {
  defaultNodeConfiguration: NodeConfiguration;
  nodes: Node[];
}

interface NodeConfiguration {
  service: "deap" | "jmetal";
  mutation: {
    probability: number;
  };
  crossover: {
    probability: number;
  };
}

interface Node {
  id: string;
  configuration?: NodeConfiguration;
  edges: string[];
}