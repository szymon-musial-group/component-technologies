import { Router } from "express";
import { ReportsRepository } from "../persistance/reports/reportsRepository";
import logger from "../utils/logger";

export const createReportController = (repository: ReportsRepository) => {
  const isIdValid = (id: string) => {
    if (!id.match(/^[0-9a-f]{24}$/)) {
      return false;
    }
    return true;
  };

  const router = Router();
  router.get("/:id", async (req, res) => {
    const id = req.params.id;
    if (!isIdValid(id)) {
      logger.warn(`reports /GET Provided id (${id}) is not valid`);
      return res.status(401).send({ error: "Id is not valid" });
    }
    const report = await repository.getReport(id);
    if (!report) {
      return res.status(404).send({ error: "Cannot find report" });
    }
    res.send(report);
  });
  return router;
};
