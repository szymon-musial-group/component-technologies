import { Topology } from "../types/configuration";

export class Migrator {
  migrate(populations: { [k: string]: any[] }, topology: Topology) {
    const migrations: { [k: string]: any[] } = {};
    const newPopulations: { [k: string]: any[] } = {};

    for (const node of topology.nodes) {
      const { migration, population } = this.selectMigrated(
        populations[node.id]
      );
      newPopulations[node.id] = population;
      const distributedPopulation = this.distributePopulation(
        migration,
        node.edges.length
      );

      for (const index in distributedPopulation) {
        const otherId = node.edges[index];
        if (!migrations[otherId]) {
          migrations[otherId] = [];
        }
        migrations[otherId].push(...distributedPopulation[index]);
      }
    }

    for (const { id } of topology.nodes) {
      newPopulations[id].push(...migrations[id]);
    }

    return newPopulations;
  }

  private selectMigrated(population: any[]): {
    migration: any[];
    population: any[];
  } {
    const migration = population.splice(
      0,
      Math.floor(Math.random() * (population.length / 2))
    );

    return { migration, population };
  }

  private distributePopulation(
    population: any[],
    bucketsNumber: number
  ): any[][] {
    const buckets: any[][] = new Array(bucketsNumber).fill([]);

    for (const individual of population) {
      buckets[Math.floor(Math.random() * bucketsNumber)].push(individual);
    }

    return buckets;
  }
}
