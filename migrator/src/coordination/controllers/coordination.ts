import { Connection } from "amqplib";
import { CoordinationService } from "../coordinationService";
import { workPortionSchema } from "../controllers/validation/schema"
import logger from "../../utils/logger";

export const useCoordinatorRabbitMQController = async (
  connection: Connection,
  service: CoordinationService
) => {
  const channel = await connection.createChannel();

  await channel.consume(
    "migrator.work",
    async (message) => {
      let headers;
      let body;

      try {
        headers = message.properties.headers;
        body = JSON.parse(message.content.toString());
      } catch {
        return logger.error("Cannot parse message as JSON");
      }

      const result = workPortionSchema.validate(body);
      
      if (result.error) {
        logger.error(`migrator.work ${result.error}`);
        return;
      }

      logger.info(`Get procces work message`);
      await service.addProcessedWork(headers.id, headers.nodeId, {
        bestResult: body.best_result,
        bestScore: body.best_score,
        population: body.population,
      });
    },
    { noAck: true }
  );
};
