import Joi from "joi";

export const workPortionSchema = Joi.object().keys({
  problem: Joi.any().optional(),
  algorithm: Joi.any().optional(),
  best_score: Joi.number().required(),
  best_result: Joi.array().items(Joi.any()).required(),
  population: Joi.array().items(Joi.any()).required(),
})