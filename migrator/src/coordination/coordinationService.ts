import { StateRepository } from "../persistance/state/stateRepository";
import { Propagator } from "../propagator/propagator";
import { Migrator } from "../migrations/migrator";
import logger from "../utils/logger";
import { AlgorithmState, WorkPortion } from "../persistance/state/algorithmState";

export class CoordinationService {
  private migrator = new Migrator();

  constructor(
    private propagator: Propagator,
    private stateRepository: StateRepository
  ) {}
  async addProcessedWork(id: string, nodeId: string, portion: WorkPortion) {
    const state = AlgorithmState.fromEntity(await this.stateRepository.get(id));
    if (!state) {
      logger.error(`State with id: ${id} not initialized`);
      return;
    }

    logger.info(`add population ${id} ${nodeId}`);
    state.addWork(nodeId, portion);
    this.stateRepository.update(state.toEntity());
    if (!state.isStageComplete()) return;
    if (state.iteration === 10) return;

    const { topology } = state.configuration;
    const populations = state.getPopulations();

    const newPopulations = this.migrator.migrate(populations, topology);

    for (const node of topology.nodes) {
      let destination = "";

      if (node.configuration) {
        destination = node.configuration.service;
      } else {
        destination = topology.defaultNodeConfiguration.service;
      }

      this.propagator.sendWorkPortion(destination, {
        algorithm: state.configuration.algorithm,
        problem: state.configuration.problem,
        id: state.configuration.id,
        population: newPopulations[node.id],
        nodeId: node.id,
      });
    }
    state.finalizeStage();
    this.stateRepository.update(state.toEntity());
    logger.info(`Iteration ${state.iteration} done!!!`);
  }
}
