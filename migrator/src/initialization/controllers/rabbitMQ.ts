import { Connection } from "amqplib";
import { InitializationService } from "../initializationService";
import { algorithmConfigurationSchema } from "../controllers/validation/schema"
import logger from "../../utils/logger";

export const useInitializationRabbitMQController = async (
  connection: Connection,
  service: InitializationService
) => {
  const channel = await connection.createChannel();

  await channel.consume(
    "migrator.initialization",
    (message) => {
      let headers;
      let body;

      try {
        headers = message.properties.headers;
        body = JSON.parse(message.content.toString());
      } catch {
        return logger.error("Cannot parse message as JSON");
      }

      const result = algorithmConfigurationSchema.validate(body);
      
      if (result.error) {
        logger.error(`migrator.initialization ${result.error}`);
        return;
      }

      logger.info(`Get initialization message`);
      service.initializeAlgorithm({ ...body, id: headers.id });
    },
    { noAck: true }
  );
};
