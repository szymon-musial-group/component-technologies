import Joi from "joi";

const nodeConf = Joi.object().keys({
  service: Joi.string().required(),
  mutation: Joi.object().keys({
    probability: Joi.number().required()
  }),
  crossover: {
    probability: Joi.number()
  }
})

export const algorithmConfigurationSchema = Joi.object().keys({ 
  id: Joi.any().optional(),
  problem: Joi.any().required(),
  algorithm: Joi.any().required(),
  topology: Joi.object().keys({
    defaultNodeConfiguration: nodeConf,
    nodes: Joi.array().items(Joi.object().keys({ 
      id: Joi.string().required(),
      configuration: nodeConf.optional(),
      edges: Joi.array().items(Joi.string()).required()
    })).required() 
  })
}); 