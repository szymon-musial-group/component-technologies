import logger from "../utils/logger";
import { AlgorithmConfiguration } from "../types/configuration";
import { Propagator } from "../propagator/propagator";
import { StateRepository } from "../persistance/state/stateRepository";
import { AlgorithmState } from "../persistance/state/algorithmState";

export class InitializationService {
  constructor(
    private propagator: Propagator,
    private stateRepository: StateRepository
  ) {}

  initializeAlgorithm = async (configuration: AlgorithmConfiguration) => {
    const { topology, id } = configuration;

    if (!topology) {
      return logger.warn(`Topology is required in configuration`);
    }

    if (await this.stateRepository.get(id)) {
      return logger.warn(`Algorithm with id: ${id} already initialized`);
    }

    const state = new AlgorithmState(configuration);
    try {
      await this.stateRepository.add(state.toEntity());
    } catch {
      logger.error("Cannot start algorithm");
      return;
    }

    for (const node of topology.nodes) {
      let destination = "";

      if (node.configuration) {
        destination = node.configuration.service;
      } else {
        destination = topology.defaultNodeConfiguration.service;
      }

      this.propagator.sendWorkPortion(destination, {
        algorithm: configuration.algorithm,
        problem: configuration.problem,
        id: configuration.id,
        nodeId: node.id,
      });
    }
  };
}
