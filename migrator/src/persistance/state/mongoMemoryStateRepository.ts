import logger from "../../utils/logger";
import { Collection, ObjectId } from "mongodb";
import { StateRepository, AlgorithmStateEntity } from "./stateRepository";

export class MongoStateRepository implements StateRepository {
  constructor(private collection: Collection) {}

  private parseId(id: string) {
    try {
      return new ObjectId(id);
    } catch (error) {
      logger.error(error.message);
    }
    return null;
  }

  async get(id: string) {
    const parsedId = this.parseId(id);
    if (!parsedId) return;
    return this.collection.findOne({
      _id: parsedId,
    }) as unknown as Promise<AlgorithmStateEntity>;
  }
  async add(state: AlgorithmStateEntity) {
    const parsedId = this.parseId(state.configuration.id);
    if (!parsedId) throw new Error();
    if (await this.collection.findOne({ _id: parsedId })) {
      logger.warn("Cannot add algorithm state, already exists");
      return;
    }
    this.collection.insertOne({ _id: parsedId, ...state });
  }
  async update(state: AlgorithmStateEntity) {
    const parsedId = this.parseId(state.configuration.id);
    if (!parsedId) throw new Error();
    this.collection.updateOne(
      { _id: parsedId },
      { $set: { _id: parsedId, ...state } }
    );
  }
}
