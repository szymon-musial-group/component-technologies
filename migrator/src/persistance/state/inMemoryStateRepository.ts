import logger from "../../utils/logger";
import { AlgorithmState } from "./algorithmState";
import { StateRepository, AlgorithmStateEntity } from "./stateRepository";

export class InMemoryStateRepository implements StateRepository {
  private states = new Map<string, AlgorithmStateEntity>();

  async get(id: string) {
    if (!this.states.has(id)) return null;
    return this.states.get(id);
  }
  async add(state: AlgorithmStateEntity) {
    if (this.get(state.configuration.id)) {
      logger.warn("Cannot add algorithm state, already exists");
      return;
    }
    this.states.set(state.configuration.id, state);
  }
  async update(state: AlgorithmStateEntity) {
    this.states.set(state.configuration.id, state);
  }
}
