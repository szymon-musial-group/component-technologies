import { AlgorithmConfiguration } from "../../types/configuration";
import { AlgorithmState, WorkPortion } from "./algorithmState";

export interface AlgorithmStateEntity {
  iteration: number;
  epochs: { [k: string]: WorkPortion }[];
  configuration: AlgorithmConfiguration;
}

export interface StateRepository {
  get(id: string): Promise<AlgorithmStateEntity | null>;
  add(state: AlgorithmStateEntity): Promise<void>;
  update(state: AlgorithmStateEntity): Promise<void>;
}
