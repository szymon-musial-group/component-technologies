import { AlgorithmConfiguration } from "../../types/configuration";
import { AlgorithmStateEntity } from "./stateRepository";

export interface WorkPortion {
  bestScore: number;
  bestResult: any;
  population: any[];
}

export class AlgorithmState {
  private nodesNumber;

  constructor(
    public readonly configuration: AlgorithmConfiguration,
    private actual: { [k: string]: WorkPortion } = {},
    private historic: { [k: string]: WorkPortion }[] = [],
    public iteration: number = 0
  ) {
    this.actual = actual;
    this.historic = historic;
    this.nodesNumber = configuration.topology.nodes.length;
  }

  isStageComplete() {
    return this.nodesNumber === Object.keys(this.actual).length;
  }

  addWork(nodeId: string, portion: WorkPortion) {
    this.actual[nodeId] = portion;
  }

  getPopulations() {
    return Object.fromEntries(
      Object.entries(this.actual).map(([nodeId, workPortion]) => [
        nodeId,
        workPortion.population,
      ])
    );
  }

  finalizeStage() {
    this.historic.push(this.actual);
    this.actual = {};
    this.iteration += 1;
  }

  static fromEntity = (entity: AlgorithmStateEntity) => {
    const { configuration, iteration, epochs } = entity;
    const actual = epochs.pop();
    return new AlgorithmState(configuration, actual, epochs, iteration);
  };

  toEntity = (): AlgorithmStateEntity => {
    const { configuration, iteration, actual, historic } = this;

    return {
      configuration: configuration,
      iteration: iteration,
      epochs: [...historic, actual],
    };
  };
}
