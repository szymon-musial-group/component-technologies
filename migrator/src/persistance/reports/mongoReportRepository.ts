import { Collection, ObjectId } from "mongodb";
import { ReportsRepository } from "./reportsRepository";
import logger from "../../utils/logger";

export class MongoReportsRepository implements ReportsRepository {
  constructor(private collection: Collection) {}

  private parseId(id: string) {
    try {
      return new ObjectId(id);
    } catch (error) {
      logger.error(error.message);
    }
    return null;
  }

  getReport = (id: string) => {
    const parsedId = this.parseId(id);
    if (!parsedId) return null;

    return this.collection.findOne({
      _id: parsedId,
    });
  };
}
