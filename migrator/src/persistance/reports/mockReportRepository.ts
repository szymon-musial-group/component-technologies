import logger from "../../utils/logger";
import { mockedReport } from "./mock";
import { ReportsRepository } from "./reportsRepository";

export class MockReportRepository implements ReportsRepository {
  getReport = (id: string) => {
    logger.warn("Report returned from mock repository!!!");
    return new Promise((resolve) => resolve(mockedReport));
  };
}
