export interface ReportsRepository {
  getReport: (id: string) => Promise<any>;
}
