import { config } from "dotenv";
config();
export const MongoDBConfiguration = {
  address: process.env.MONGODB_ADDRESS || "mongodb",
  port: parseInt(process.env.MONGODB_PORT || "27017"),
  database: process.env.MONGODB_DATABASE || "state",
  user: process.env.MONGODB_USER || "admin",
  password: process.env.MONGODB_PASSWORD || "admin",
};
