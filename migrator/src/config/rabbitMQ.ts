import { config } from "dotenv";
config();
export const RabbitMQConfiguration = {
  hostname: process.env.RABBITMQ_HOST || "rabbitmq",
  port: parseInt(process.env.RABBITMQ_PORT || "5672"),
  password: process.env.RABBITMQ_USER || "admin",
  username: process.env.RABBITMQ_PASS || "admin",
};
