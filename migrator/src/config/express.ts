import { config } from "dotenv";
config();
export const ExpressConfiguration = {
  mock: process.env.REPORTS_MOCK == "true" ? true : false,
  port: process.env.EXPRESS_PORT || "8001",
};
