const NodemonPlugin = require('nodemon-webpack-plugin');
const path = require('path');
const webpack = require('webpack')

module.exports = {
  mode: 'production',
  entry: './src/main.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js',
  },
  stats: 'minimal',
  watchOptions:{
    ignored: './dist'
  },
  target: 'node',
  resolve: {
    extensions: ['.js', '.json', '.ts'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: 'ts-loader',
      },
    ],
  },
  plugins: [
    new webpack.WatchIgnorePlugin({
        paths: ["./dist"]
    })
  ],
};
