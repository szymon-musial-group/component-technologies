#!/usr/bin/env python
import sys, os, jsonpickle
from config import Config
from ctcommons.rabbitmq.config import RabbitMQConfig
from ctcommons.rabbitmq.client import RabbitMQClient
from datetime import datetime

def appendIdFromHeadersIfExists(obj, properties):
    if properties.headers and 'id' in properties.headers:
        obj['_id'] = properties.headers['id']

def try_to_insert_as_json(channel, method, properties, body, mongo_collection) -> bool:
    try:            
        obj = jsonpickle.loads(body)
        obj['_time'] = datetime.now().isoformat()
        appendIdFromHeadersIfExists(obj, properties)
        mongo_collection.insert_one(obj)
        channel.basic_ack(method.delivery_tag)
        return True
    except:
        return False

def try_to_insert_as_string(channel, method, properties, body, mongo_collection) -> bool:
    try:            
        string = str(body, 'UTF-8')
        obj = {
            'string': string,
            '_time': datetime.now().isoformat()            
        }
        appendIdFromHeadersIfExists(obj, properties)        
        mongo_collection.insert_one(obj)
        channel.basic_ack(method.delivery_tag)
        return True
    except:
        return False

def process_incoming_message(channel, method, properties, body, mongo_collection):
    print(f"-----{method.delivery_tag}-----")
    print(f" [channel] {channel}")
    print(f" [method] {method}")
    print(f" [properties ] {properties}")
    print(f" [body ] {body}")
    
    if try_to_insert_as_json(channel, method, properties, body, mongo_collection):
        return
    
    if try_to_insert_as_string(channel, method, properties, body, mongo_collection):
        return
   
    channel.basic_reject(method.delivery_tag)
    print(f" Rejected {body}")

def main():
    config = Config()
    rabbitmq_config = RabbitMQConfig()
    rabbitmq_client = RabbitMQClient(rabbitmq_config.generate_Pika_ConnectionParameters())
    channel = rabbitmq_client.open_channel()

    mongo_client = config.generate_pymongo_MongoClient()    
    mydb = mongo_client[config.MONGO_DB]
    mycol = mydb[config.MONGO_COLLECTION]

    print("Existing databases: ")
    print(mongo_client.list_database_names())    

    def callback(channel, method, properties, body):
        process_incoming_message(channel, method, properties, body, mycol)

    channel.basic_consume(queue='results', on_message_callback=callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
