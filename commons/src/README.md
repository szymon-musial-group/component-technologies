# Component Technologies Commons

The Commons package provides a set of common functionalities for establishing and managing connections with RabbitMQ for
component technologies project.

## Installation

You can install the package via pip:

```bash
pip install ctcommons
```

## Usage

### RabbitMQConfig

Connection with RabbitMQ is configured using environment variables:

- self.RABBITMQ_USER
- self.RABBITMQ_PASS
- self.RABBITMQ_HOST - localhost if not provided
- self.RABBITMQ_PORT - 5672 if not provided

### RabbitMQClient

Expects pika connection parameters, which should be generated using `RabbitMQConfig` and allows opening a channel.

### WorkerNodeController

Template for workers such as DEAP and JMetal services. Its `handle_message` method should be used as a callback for
RabbitMQ messages. Automatically forwards result to the next hop queue, specified in the `path` header.

## Development

To publish new version, do the following:

- Update the `version` in `setup.py`
- Make sure that You have `wheels` installed on Your venv
- Build the package

```bash
python setup.py bdist_wheel sdist
```

- To install and test locally, run

```bash
pip install .
```

- To publish new version, You should use `twine`
- To check that package is built properly, run

```bash
twine check dist/*
```

- Upload package to testPyPi

```bash
twine upload -r testpypi dist/*
```

- This command will prompt username and login, so You should prepare package according to previous steps on separate
  branch and contact me, so that I could run last command and upload new version 