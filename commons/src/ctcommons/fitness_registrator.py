from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry import metrics
from opentelemetry.exporter.otlp.proto.grpc.metric_exporter import OTLPMetricExporter
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader

import os, platform
import time


class FitnessRegistrator:
    def __init__(self):
        service_name = platform.uname()[1]
        otel_endpoint = os.getenv("OTEL_ENDPOINT")

        resource = Resource(attributes={SERVICE_NAME: service_name})
        reader = PeriodicExportingMetricReader(
            OTLPMetricExporter(endpoint=otel_endpoint, insecure=True),
        )

        meter_provider = MeterProvider(resource=resource, metric_readers=[reader])
        metrics.set_meter_provider(meter_provider)
        meter = meter_provider.get_meter(__name__)
        self.reader = reader

        self.counter = meter.create_up_down_counter(
            name=f"fitness_counter",
            description="Includes values of fitness obtained during algorithm run",
            unit="fitness",
        )
        print("created OTEL meter")
        self.last_fitness=None # hakerka
        self.job_id = None  # this is set by WorkerNodeController

    def register(self, fitness, firstRegister = False):
        if firstRegister:
            diff = fitness
        else:            
            diff = self.last_fitness - fitness
        
        self.last_fitness = fitness
        
        self.counter.add(diff, attributes={"job_id": self.job_id})
        self.reader.force_flush()
        time.sleep(0.05)  # just to make things a little bit slower
