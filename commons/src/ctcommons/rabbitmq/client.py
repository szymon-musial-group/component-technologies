import pika
from pika.adapters.blocking_connection import BlockingChannel


class RabbitMQClient:
    def __init__(self, connection_parameters: pika.ConnectionParameters):
        self.connection_parameters = connection_parameters
        self.connection = pika.BlockingConnection(connection_parameters)

    def open_channel(self) -> BlockingChannel:
        return self.connection.channel()

    def close_connection(self):
        self.connection.close()
