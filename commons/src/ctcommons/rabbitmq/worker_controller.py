import json

import jsonschema
import pika

from ctcommons.fitness_registrator import FitnessRegistrator

header_schema = {
    "type": "object",
    "properties": {
        "id": {"type": "string"},
        "nodeId": {"type": "string"},
        "path": {
            "type": "array",
            "items": {"type": "string"},
        },
    },
    "required": ["id", "path"],
}


class WorkerNodeController:
    def __init__(self, handle_job: callable):
        self.handle_job = handle_job
        self.fitness_registrator = FitnessRegistrator()

    def handle_message(self, channel, method, properties, body):
        headers = properties.headers
        try:
            jsonschema.validate(instance=headers, schema=header_schema)

            next_hop, next_hop_headers = self._prepare_next_hop_data(headers)
            parsed = json.loads(body)

            print(f"Start processing message {headers}")
            self.fitness_registrator.job_id = next_hop_headers["id"]
            result = self.handle_job(parsed, self.fitness_registrator)

            new_population = result["solutions"]
            best_score = result["best_score"]
            best_result = result["best_result"]

            print(
                "Processed work portion ( id:{}, path: {}, next_hop: {} ). Result score {}".format(
                    next_hop_headers["id"],
                    next_hop_headers["path"],
                    next_hop,
                    best_score,
                )
            )
            channel.basic_ack(method.delivery_tag)
            channel.basic_publish(
                "",
                next_hop,
                json.dumps(
                    {
                        "problem": parsed["problem"],
                        "algorithm": parsed["algorithm"],
                        "population": new_population,
                        "best_score": best_score,
                        "best_result": best_result,
                    }
                ),
                properties=pika.BasicProperties(headers=next_hop_headers),
            )
            return

        except Exception as e:
            print(f"Cannot process message!!! get error {e}")
            channel.basic_reject(method.delivery_tag)

    def _prepare_next_hop_data(self, headers):
        _id = headers["id"]
        path = headers["path"]

        next_hop = "results"
        if path:
            next_hop = path[0]
            path = path[1:]

        next_hop_headers = {"id": _id, "path": path}
        if "nodeId" in headers:
            next_hop_headers["nodeId"] = headers["nodeId"]

        return next_hop, next_hop_headers
