from setuptools import find_packages, setup

with open("src/README.md", "r") as f:
    long_description = f.read()

setup(
    name="ctcommons",
    version="0.0.6",
    description="A library containing common functionalities for component technologies project.",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="1marcin100",
    install_requires=[
        "pika >= 1.3.2",
        "python-dotenv >= 1.0.0",
        "jsonschema >= 4.19.2",
        "opentelemetry-api >= 1.22.0",
        "opentelemetry-sdk >= 1.22.0",
        "opentelemetry-exporter-otlp-proto-grpc >= 1.22.0",
    ],
    python_requires=">=3.10",
)
