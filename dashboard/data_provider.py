import json

import numpy as np
from coordinates_from_distance_matrix import get_coordinates_from_distance_matrix
from styles.color_utils import (
    get_pastel_bg_node,
    get_pastel_border_node,
    get_pastel_color_edge,
)

f = open("./demo_data")
data = json.load(f)
distance_matrix = np.array(data["problem"]["parameters"]["distanceMatrix"])
no_cities = len(distance_matrix)


city_positions = get_coordinates_from_distance_matrix(distance_matrix)

population = np.array(data["population"])

rows, cols = (no_cities, no_cities)
POPULATION_SUMMARY = [[0 for i in range(cols)] for j in range(rows)]

for individual in population:
    previous_city = individual[len(individual) - 1]
    for current_city in individual:
        POPULATION_SUMMARY[previous_city][current_city] += 1
        previous_city = current_city


ELEMENTS = []
# define graph nodes
for i in range(0, no_cities):
    ELEMENTS.append(
        {
            "data": {
                "id": str(i),
                "label": str(i),
                "bg_color": get_pastel_bg_node(i),
                "border_color": get_pastel_border_node(i),
            },
            "position": {"x": city_positions[i][0], "y": city_positions[i][1]},
        }
    )

for i in range(0, no_cities):
    for j in range(0, no_cities):
        if i < j:
            edge_popularity = POPULATION_SUMMARY[i][j] + POPULATION_SUMMARY[j][i]
            ELEMENTS.append(
                {
                    "data": {
                        "source": str(i),
                        "target": str(j),
                        "weight": distance_matrix[i][j],
                        "line_color": get_pastel_color_edge(i, j),
                        "edge_popularity": edge_popularity,
                    }
                }
            )

NODES_HASH = []
