import dash_cytoscape as cyto
from cytoscape_map import cytoscape_map_map
from dash import Dash, Input, Output, State, callback, dcc, html
from data_provider import no_cities
from styles.cytoscape_styles import STYLESHEETS, hide_edges_with_popularity_below

cyto.load_extra_layouts()


app = Dash(__name__)
app.config.suppress_callback_exceptions = True

NAVBAR_COLOR = "#c5c7c7"
app.layout = html.Div(
    [
        html.Div(
            children=[
                html.H1(children="Traveling salesman "),
            ],
            style={},
        ),
        html.Div(
            children=[
                html.Div(
                    children=[
                        html.H1(
                            children=str(no_cities) + " cities",
                            style={"textAlign": "center"},
                        ),
                        dcc.Dropdown(
                            id="display-mode-dropdown",
                            options=[
                                {
                                    "label": "Display all connections",
                                    "value": "all_connections",
                                },
                                {
                                    "label": "Show popular paths only",
                                    "value": "pop_paths",
                                },
                                {
                                    "label": "Show path from given vertex",
                                    "value": "vertex_paths",
                                },
                            ],
                            value="all_connections",
                        ),
                        html.Div(
                            id="more_options",
                            style={
                                "width": 300,
                                "padding": 10,
                            },
                        ),
                    ],
                    style={
                        "backgroundColor": NAVBAR_COLOR,
                        "width": 300,
                        "padding": 10,
                    },
                ),
                html.Div(children=[cytoscape_map_map], style={"flex": 1}),
            ],
            style={"display": "flex", "flexDirection": "row"},
        ),
    ],
    style={},
)


@callback(
    Output("more_options", "children"),
    Input("display-mode-dropdown", "value"),
)
def set_tools_in_sidebar(value):
    if value == "vertex_paths":
        return html.Div(
            children=[
                dcc.Slider(0, no_cities, 1, id="slider", value=0, vertical=True),
                dcc.Markdown(
                    id="cytoscape-selectedNodeData-markdown",
                    children="",
                    style={"margin": 20},
                ),
            ],
            style={"display": "flex", "flexDirection": "row"},
        )
    if value == "pop_paths":
        return dcc.Slider(0, 5, 1, id="min_pop_path", value=1)

    return []


@callback(
    Output("city-map", "stylesheet", allow_duplicate=True),
    Input("min_pop_path", "value"),
    prevent_initial_call=True,
)
def set_minimum_popularity_to_display_path(value):
    return [
        *STYLESHEETS["pop_paths"],
        *hide_edges_with_popularity_below(value),
    ]


@callback(
    Output("city-map", "stylesheet", allow_duplicate=True),
    Input("display-mode-dropdown", "value"),
    prevent_initial_call=True,
)
def set_display_mode(value):
    if value == "all_connections":
        return STYLESHEETS["all_connections"]

    if value == "pop_paths":
        return [*STYLESHEETS["pop_paths"], *hide_edges_with_popularity_below(1)]

    if value == "vertex_paths":
        return STYLESHEETS["vertex_paths"]

    return []


@callback(
    Output("cytoscape-selectedNodeData-markdown", "children"),
    Input("city-map", "selectedNodeData"),
)
def displaySelectedNodeData(data_list):
    if data_list is None or len(data_list) == 0:
        return "⚠️ No city selected."
    selected_city = data_list[0].get("id", "edge")
    return "You selected the following start city: " + selected_city


if __name__ == "__main__":
    app.run(debug=True)
