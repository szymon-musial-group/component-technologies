import dash_cytoscape as cyto
import numpy as np
from dash import Input, Output, State, callback
from data_provider import ELEMENTS, POPULATION_SUMMARY, no_cities
from styles.cytoscape_styles import BG_COLOR, STYLESHEETS, edge_style

cytoscape_map_map = cyto.Cytoscape(
    id="city-map",
    elements=ELEMENTS,
    layout={
        "name": "spread",
        "prelayout": {"name": "preset"},
    },  # slightly move nodes apart to reduce covering some of them
    style={"width": "100%", "height": "750px", "background": BG_COLOR},
    stylesheet=STYLESHEETS["all_connections"],
)


@callback(
    Output("city-map", "elements", allow_duplicate=True),
    Input("slider", "value"),
    State("city-map", "selectedNodeData"),
    prevent_initial_call=True,
)
def highlight_accessible_nodes(slider_value, data_list):
    if data_list is None or len(data_list) == 0:
        return ELEMENTS

    start_city = int(data_list[0]["id"])

    distance = slider_value
    min_popularity = 2

    all_accessible_nodes = [start_city]
    prev_accessible_nodes = [start_city]

    for elem in ELEMENTS:
        elem["classes"] = None

    distance_colors = [
        "#e39b7f",
        "#e3b17f",
        "#e3c37f",
        "#ede592",
        "#d9ed92",
        "#b5e48c",
        "#99d98c",
        "#76c893",
        "#52b69a",
        "#34a0a4",
        "#168aad",
        "#1a759f",
        "#1e6091",
        "#184e77",
        "#183477",
        "#182277",
        "#3d2280",
        "#6c338f",
        "#9e469e",
        "#c777af",
        "#cc8787",
    ]
    while distance > 0:
        distance -= 1
        for prev_city in prev_accessible_nodes:
            new_accessible_nodes = []
            for elem in ELEMENTS:
                element_is_node = elem["data"].get("id", None) is not None
                if element_is_node:
                    pass
                else:
                    target = int(elem["data"].get("target", None))
                    source = int(elem["data"].get("source", None))

                    if target == prev_city or source == prev_city:
                        city = target if prev_city == source else source
                        class_name = (
                            "arrow_target" if source == prev_city else "arrow_source"
                        )

                        if (
                            city not in all_accessible_nodes
                            and POPULATION_SUMMARY[city][prev_city]
                            + POPULATION_SUMMARY[prev_city][city]
                            >= min_popularity
                        ):
                            elem["data"]["distance_label"] = slider_value - distance
                            elem["data"]["distance_color"] = distance_colors[
                                slider_value - distance
                            ]
                            elem["classes"] = class_name
                            new_accessible_nodes.append(city)
        all_accessible_nodes.extend(new_accessible_nodes)
        prev_accessible_nodes = new_accessible_nodes

    return ELEMENTS
