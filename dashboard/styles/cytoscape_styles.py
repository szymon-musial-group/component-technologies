BG_COLOR = "#F4F6F6"


edge_style = {
    "width": 1,
    "text-background-opacity": 1,
    "text-background-color": BG_COLOR,
    "line-color": "data(line_color)",
}

arrow_style = {
    "arrow-scale": 2,
    "line-color": "data(distance_color)",
    "label": "data(distance_label)",
    "font-size": 25,
    "width": "2",
}

NODE_STYLE = [
    {
        "selector": "node",
        "style": {
            "background-color": "data(bg_color)",
            "border-color": "data(border_color)",
            "border-width": "3",
            "content": "data(label)",
            "text-halign": "center",
            "text-valign": "center",
            "width": "30",
            "height": "30",
        },
    },
    {
        "selector": "node:selected",
        "style": {
            "background-color": "data(border_color)",
            "width": "40",
            "height": "40",
            "color": "white",
            "font-size": "25",
        },
    },
]

HIDE_EDGES = [
    {
        "selector": "edged",
        "style": {
            "curve-style": "bezier",
            "width": 0,
        },
    },
]

ARROW_CLASSES = [
    {
        "selector": "edge.arrow_source",
        "style": {
            **arrow_style,
            "source-arrow-shape": "triangle",
            "source-arrow-color": "data(distance_color)",
        },
    },
    {
        "selector": "edge.arrow_target",
        "style": {
            **arrow_style,
            "target-arrow-shape": "triangle",
            "target-arrow-color": "data(distance_color)",
        },
    },
]

ALL_CONNECTIONS = [
    {
        "selector": "edge:unselected",
        "style": {
            **edge_style,
            "label": "data(weight)",
        },
    },
    {
        "selector": "edge:selected",
        "style": {
            **edge_style,
            "font-size": 25,
            "width": 5,
        },
    },
]


def hide_edges_with_popularity_below(min_popularity):
    return [
        {
            "selector": "[edge_popularity < " + str(min_popularity) + "]",
            "style": {"width": 0},
        },
    ]


POP_PATHS = [
    {
        "selector": "edge:unselected",
        "style": {
            **edge_style,
            # "width": "data(edge_popularity)",
            "label": "data(edge_popularity)",
        },
    },
    {
        "selector": "edge:selected",
        "style": {
            **edge_style,
            "font-size": 25,
            "width": 5,
        },
    },
]

STYLESHEETS = {
    "all_connections": [*ALL_CONNECTIONS, *NODE_STYLE],
    "pop_paths": [*POP_PATHS, *NODE_STYLE],
    "vertex_paths": [*HIDE_EDGES, *ARROW_CLASSES, *NODE_STYLE],
}
