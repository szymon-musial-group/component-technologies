bg_colors = [
    "#7EE5E5",
    "#E5DE7E",
    "#EEB4DD",
    "#e0958b",
    "#eba742",
    "#B2E8C2",
    "#ffd4d1",
    "#f7f1ad",
    "#cff2ae",
    "#B1B8E8",
    "#d19ae6",
]


def get_pastel_bg_node(i):
    return bg_colors[i % len(bg_colors)]


def get_pastel_border_node(i):
    return blendRGBColors(get_pastel_bg_node(i), "#000000", 0.5)


def HEXtoRGB(hex):
    h = hex.lstrip("#")
    return [int(h[i : i + 2], 16) for i in (0, 2, 4)]


def RGBtoHEX(rgb):
    r, g, b = rgb
    return "#{:02x}{:02x}{:02x}".format(r, g, b)


def blendRGBColors(rgb1_hex, rgb2_hex, ratio=1):
    rgb1, rgb2 = HEXtoRGB(rgb1_hex), HEXtoRGB(rgb2_hex)
    new_color = [int((2 - ratio) * rgb1[i] + ratio * rgb2[i]) // 2 for i in [0, 1, 2]]
    return RGBtoHEX(new_color)


def get_pastel_color_edge(i, j):
    # when edge color equals mean value of node values the graph looks less cluttered
    source_node_color = get_pastel_border_node(i)
    target_node_color = get_pastel_border_node(j)
    mean_color = blendRGBColors(source_node_color, target_node_color)
    return mean_color
