import math


def get_coordinates_from_distance_matrix(distance_matrix):
    no_cities = len(distance_matrix)
    city_positions = [[None, None] for x in range(0, no_cities)]

    # We need to choose the position of the first three cities to unambiguously determine positions of remaining cities
    # assume that two first cities are in a single line
    city_positions[0] = [0, 0]
    city_positions[1] = [0, distance_matrix[0][1]]

    def eval_coordinates_with_distance_from_first_two_cities(
        distance_to_city_0, distance_to_city_1
    ):
        city1_y = city_positions[1][1]
        city_y = (
            distance_to_city_1**2 - distance_to_city_0**2 - city1_y * city1_y
        ) / (-2 * city1_y)
        city_x = math.sqrt(
            distance_to_city_0**2 - city_y * city_y
        )  # this has two solutions
        return [city_x, city_y]

    # calculate the third city position
    city_positions[2] = eval_coordinates_with_distance_from_first_two_cities(
        distance_matrix[0][2], distance_matrix[1][2]
    )

    # thats why we had to start with 3 cities
    def check_if_x_should_be_negative(city_x, city_y, act_distance_to_city_2):
        city2_x = city_positions[2][0]
        city2_y = city_positions[2][1]

        distance_to_city_2 = math.sqrt(
            (city_x - city2_x) ** 2 + (city_y - city2_y) ** 2
        )
        distance_to_city_2_after_x_negation = math.sqrt(
            (-city_x - city2_x) ** 2 + (city_y - city2_y) ** 2
        )

        return (
            False
            if abs(distance_to_city_2 - act_distance_to_city_2)
            < abs(distance_to_city_2_after_x_negation - act_distance_to_city_2)
            else True
        )

    for idx in range(3, no_cities):
        new_x, new_y = eval_coordinates_with_distance_from_first_two_cities(
            distance_matrix[0][idx], distance_matrix[1][idx]
        )
        swap_x = check_if_x_should_be_negative(new_x, new_y, distance_matrix[2][idx])

        city_positions[idx] = [new_x if not swap_x else -new_x, new_y]

    return city_positions
