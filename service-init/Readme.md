# Service-init

This is a Fast API application which exposes http API to start new jobs based on provided configuration.

## Requirements

- Python 3.10 or higher
- Poetry
- Docker

## Setup

1. Run `poetry install` to fetch the dependencies
2. Run `poetry run pre-commit install` to setup pre-commit hooks

## Development

It's needed that rabbitMQ and mongo database are running as specified in `app/config.py`

1. Run `poetry shell` to enter poetry shell
2. Execute  
   `uvicorn app.main:app --reload`  
   to start up the service
3. Check the docs and example requests using `http://localhost:8000/docs`
4. Execute request in `rest` directory (using e.g. vscode rest-client extension)

## Docker

1. Build image with following command:  
   `docker build -t service-init .`
2. Assuming network `init` exists and both `rabbitmq` and `mongodb` containers are running, run the service with:  
   `docker run --network=init -e RABBITMQ_HOST=rabbitmq -e MONGO_HOST=mongodb --name service-init -p 6868:6868 service-init`
3. Remove container using commands:  
   `docker stop service-init`  
   `docker rm service-init`

4. If you want to start container which already exists, just run  
   `docker start service-init`
