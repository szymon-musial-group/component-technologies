from contextlib import asynccontextmanager

import pika
from dotenv import load_dotenv
from fastapi import FastAPI

from .config import Config
from .routers import jobs
from ctcommons.rabbitmq.config import RabbitMQConfig


@asynccontextmanager
async def lifespan(app: FastAPI):
    config = Config()
    rabbitmq_config = RabbitMQConfig()

    load_dotenv()

    app.mongo_client = config.generate_pymongo_MongoClient()
    app.mongo_db = app.mongo_client[config.MONGO_DB]
    app.mongo_collection = app.mongo_db[config.MONGO_COLLECTION]
    print("connected to mongodb")

    app.rabbit_client = pika.BlockingConnection(
        rabbitmq_config.generate_Pika_ConnectionParameters()
    )
    app.rabbit_channel = app.rabbit_client.channel()
    print("connected to rabbitMQ")

    yield

    print("closing connection to mongodb")
    app.mongo_client.close()

    print("closing connection to rabbitMQ")
    app.rabbit_client.close()


app = FastAPI(lifespan=lifespan)
app.include_router(jobs.router)
