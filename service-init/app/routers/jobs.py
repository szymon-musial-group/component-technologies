import json
from typing import Annotated

import pika
from fastapi import APIRouter, Body, Request
from fastapi.encoders import jsonable_encoder

from ..models.jobs import Job, jobs_examples
import random
import time

router = APIRouter(
    prefix="/jobs",
    tags=["jobs"],
)


@router.post("/")
async def start_new_job(
    request: Request,
    job: Annotated[Job, Body(openapi_examples=jobs_examples)],
):
    job_dict = jsonable_encoder(job)
    new_job = request.app.mongo_collection.insert_one(job_dict)
    job_id = new_job.inserted_id
    saved_job = request.app.mongo_collection.find_one({"_id": job_id})

    headers = {"id": job_id}
    rabbit_payload = job_dict.copy()
    rabbit_payload["id"] = job_id
    del rabbit_payload["name"]
    del rabbit_payload["_id"]

    if job_dict["path"] is not None:
        next_hop = job_dict["path"][0]

        path = job_dict["path"][1:]
        rabbit_payload["path"] = path
        headers["path"] = path

    else:
        next_hop = "migrator.initialization"
        del rabbit_payload["path"]

    request.app.rabbit_channel.basic_publish(
        "",
        next_hop,
        json.dumps(rabbit_payload),
        properties=pika.BasicProperties(headers=headers),
    )

    return saved_job
