import os

from dotenv import load_dotenv
from pymongo import MongoClient


class Config:
    def __init__(self):
        load_dotenv()

        self.MONGO_USER = os.getenv("MONGO_USER", "admin")
        self.MONGO_PASS = os.getenv("MONGO_PASS", "admin")
        self.MONGO_HOST = os.getenv("MONGO_HOST", "localhost")
        self.MONGO_PORT = os.getenv("MONGO_PORT", 27017)

        self.MONGO_DB = os.getenv("MONGO_DB", "results")
        self.MONGO_COLLECTION = os.getenv("MONGO_COLLECTION", "jobs")

    def generate_pymongo_MongoClient(self) -> MongoClient:
        return MongoClient(
            host=f"{self.MONGO_HOST}:{self.MONGO_PORT}",
            serverSelectionTimeoutMS=3000,
            username=self.MONGO_USER,
            password=self.MONGO_PASS,
        )
