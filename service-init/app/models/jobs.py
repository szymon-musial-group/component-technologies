import uuid
from enum import Enum
from typing import Optional

from pydantic import UUID4, BaseModel, Field


class StopCondition(BaseModel):
    ngen: int


class Operation(BaseModel):
    type: str
    parameters: dict


class Initialization(BaseModel):
    initialPopulation: int


class Algorithm(BaseModel):
    type: str
    initialization: Initialization
    selection: Operation
    crossover: Operation
    mutation: Operation
    stopCondition: StopCondition


class PathItem(str, Enum):
    jmetal = "jmetal"
    deap = "deap"


# below is used to make it look better/easier to understand in the docs
class Problem(Operation):
    pass


def uid_factory():
    return uuid.uuid4().hex[:24]


class Job(BaseModel):
    id: UUID4 = Field(default_factory=uid_factory, alias="_id")
    name: Optional[str] = None
    path: Optional[list[PathItem]] = None
    topology: Optional[dict] = None
    problem: Problem
    algorithm: Algorithm


# fmt: off
jobs_examples = {
    "example1": {
        "summary": "Travelling salesman + genetic algorithm (graph)",
        "description": "Solution of travelling salesman problem (17 cities) using standard genetic algorithm - graph version",
        "value": {
            "name": "tsp_17_graph",
            "problem": {
                "type": "tsp",
                "parameters": {
                    "tourSize": 17,
                    "optTour": [15, 11, 8, 4, 1, 9, 10, 2, 14, 13, 16, 5, 7, 6, 12, 3, 0],
                    "optDistance": 2085,
                    "distanceMatrix":
                        [[0, 633, 257, 91, 412, 150, 80, 134, 259, 505, 353, 324, 70, 211, 268, 246, 121],
                         [633, 0, 390, 661, 227, 488, 572, 530, 555, 289, 282, 638, 567, 466, 420, 745, 518],
                         [257, 390, 0, 228, 169, 112, 196, 154, 372, 262, 110, 437, 191, 74, 53, 472, 142],
                         [91, 661, 228, 0, 383, 120, 77, 105, 175, 476, 324, 240, 27, 182, 239, 237, 84],
                         [412, 227, 169, 383, 0, 267, 351, 309, 338, 196, 61, 421, 346, 243, 199, 528, 297],
                         [150, 488, 112, 120, 267, 0, 63, 34, 264, 360, 208, 329, 83, 105, 123, 364, 35],
                         [80, 572, 196, 77, 351, 63, 0, 29, 232, 444, 292, 297, 47, 150, 207, 332, 29],
                         [134, 530, 154, 105, 309, 34, 29, 0, 249, 402, 250, 314, 68, 108, 165, 349, 36],
                         [259, 555, 372, 175, 338, 264, 232, 249, 0, 495, 352, 95, 189, 326, 383, 202, 236],
                         [505, 289, 262, 476, 196, 360, 444, 402, 495, 0, 154, 578, 439, 336, 240, 685, 390],
                         [353, 282, 110, 324, 61, 208, 292, 250, 352, 154, 0, 435, 287, 184, 140, 542, 238],
                         [324, 638, 437, 240, 421, 329, 297, 314, 95, 578, 435, 0, 254, 391, 448, 157, 301],
                         [70, 567, 191, 27, 346, 83, 47, 68, 189, 439, 287, 254, 0, 145, 202, 289, 55],
                         [211, 466, 74, 182, 243, 105, 150, 108, 326, 336, 184, 391, 145, 0, 57, 426, 96],
                         [268, 420, 53, 239, 199, 123, 207, 165, 383, 240, 140, 448, 202, 57, 0, 483, 153],
                         [246, 745, 472, 237, 528, 364, 332, 349, 202, 685, 542, 157, 289, 426, 483, 0, 336],
                         [121, 518, 142, 84, 297, 35, 29, 36, 236, 390, 238, 301, 55, 96, 153, 336, 0]]
                }
            },
            "algorithm": {
                "type": "genetic",
                "initialization": {
                    "initialPopulation": 300
                },
                "selection": {
                    "type": "selTournament",
                    "parameters": {
                        "tournsize": 3
                    }
                },
                "crossover": {
                    "type": "cxPartialyMatched",
                    "parameters": {
                        "cxpb": 0.7,
                    }
                },
                "mutation": {
                    "type": "mutShuffleIndexes",
                    "parameters": {
                        "mutpb": 0.2,
                        "indpb": 0.05
                    }
                },
                "stopCondition": {
                    "ngen": 40
                }
            },
            "topology": {
                "defaultNodeConfiguration": {
                    "service": "jmetal"
                },
                "nodes": [{
                    "id": "A",
                    "edges": ["B", "F"]
                },
                    {
                        "id": "B",
                        "edges": ["A", "C"]
                    },
                    {
                        "id": "C",
                        "edges": ["B", "D"]
                    },
                    {
                        "id": "D",
                        "edges": ["E", "C"]
                    },
                    {
                        "id": "E",
                        "edges": ["F", "D"]
                    },
                    {
                        "id": "F",
                        "edges": ["E", "A"]
                    }]
            }
        }
    },
    "example2": {
        "summary": "Travelling salesman + genetic algorithm (path)",
        "description": "Solution of travelling salesman problem (17 cities) using standard genetic algorithm - path version",
        "value": {
            "name": "tsp_17_path",
            "path": ["jmetal", "deap"],
            "problem": {
                "type": "tsp",
                "parameters": {
                    "tourSize": 17,
                    "optTour": [15, 11, 8, 4, 1, 9, 10, 2, 14, 13, 16, 5, 7, 6, 12, 3, 0],
                    "optDistance": 2085,
                    "distanceMatrix":
                        [[0, 633, 257, 91, 412, 150, 80, 134, 259, 505, 353, 324, 70, 211, 268, 246, 121],
                         [633, 0, 390, 661, 227, 488, 572, 530, 555, 289, 282, 638, 567, 466, 420, 745, 518],
                         [257, 390, 0, 228, 169, 112, 196, 154, 372, 262, 110, 437, 191, 74, 53, 472, 142],
                         [91, 661, 228, 0, 383, 120, 77, 105, 175, 476, 324, 240, 27, 182, 239, 237, 84],
                         [412, 227, 169, 383, 0, 267, 351, 309, 338, 196, 61, 421, 346, 243, 199, 528, 297],
                         [150, 488, 112, 120, 267, 0, 63, 34, 264, 360, 208, 329, 83, 105, 123, 364, 35],
                         [80, 572, 196, 77, 351, 63, 0, 29, 232, 444, 292, 297, 47, 150, 207, 332, 29],
                         [134, 530, 154, 105, 309, 34, 29, 0, 249, 402, 250, 314, 68, 108, 165, 349, 36],
                         [259, 555, 372, 175, 338, 264, 232, 249, 0, 495, 352, 95, 189, 326, 383, 202, 236],
                         [505, 289, 262, 476, 196, 360, 444, 402, 495, 0, 154, 578, 439, 336, 240, 685, 390],
                         [353, 282, 110, 324, 61, 208, 292, 250, 352, 154, 0, 435, 287, 184, 140, 542, 238],
                         [324, 638, 437, 240, 421, 329, 297, 314, 95, 578, 435, 0, 254, 391, 448, 157, 301],
                         [70, 567, 191, 27, 346, 83, 47, 68, 189, 439, 287, 254, 0, 145, 202, 289, 55],
                         [211, 466, 74, 182, 243, 105, 150, 108, 326, 336, 184, 391, 145, 0, 57, 426, 96],
                         [268, 420, 53, 239, 199, 123, 207, 165, 383, 240, 140, 448, 202, 57, 0, 483, 153],
                         [246, 745, 472, 237, 528, 364, 332, 349, 202, 685, 542, 157, 289, 426, 483, 0, 336],
                         [121, 518, 142, 84, 297, 35, 29, 36, 236, 390, 238, 301, 55, 96, 153, 336, 0]]
                }
            },
            "algorithm": {
                "type": "genetic",
                "initialization": {
                    "initialPopulation": 300
                },
                "selection": {
                    "type": "selTournament",
                    "parameters": {
                        "tournsize": 3
                    }
                },
                "crossover": {
                    "type": "cxPartialyMatched",
                    "parameters": {
                        "cxpb": 0.7,
                    }
                },
                "mutation": {
                    "type": "mutShuffleIndexes",
                    "parameters": {
                        "mutpb": 0.2,
                        "indpb": 0.05
                    }
                },
                "stopCondition": {
                    "ngen": 40
                }
            }
        }
    }
}
# fmt: on
