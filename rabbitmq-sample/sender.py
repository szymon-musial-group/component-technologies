#!/usr/bin/env python

import pika, pickle, json, uuid
from config import Config
from sample_class import SampleDataClass

def generatePropertiesWithUUID():
  return pika.BasicProperties(
    headers={'id': str(uuid.uuid1())}                          
  )


config = Config()

connection = pika.BlockingConnection(config.generate_Pika_ConnectionParameters())
channel = connection.channel()


# https://www.rabbitmq.com/tutorials/tutorial-four-python.html
channel.basic_publish(exchange='', # empty string means (AMQP default), and it's direct exchange
                      routing_key=config.RABBITMQ_QUEUE, # In this direct exchange it's means destination queue name.
                      #The default exchange is implicitly bound to every queue, with a routing key equal to the queue name.
                      # It is not possible to explicitly bind to, or unbind from the default exchange. It also cannot be deleted. 
                      properties=generatePropertiesWithUUID(),
                      body='Hello World!') # Payload

# channel.basic_publish(exchange='',
#                       routing_key=config.RABBITMQ_QUEUE,
#                       properties=pika.BasicProperties(
#                           content_type="application/x-binary"
#                       ),
#                       body=pickle.dumps(SampleClass("Wiadomość z Pikla")),
#                     )

channel.basic_publish(exchange='',
                      routing_key=config.RABBITMQ_QUEUE,
                      properties=generatePropertiesWithUUID(),
                      body=json.dumps(SampleDataClass("Wiadomość z JSON-a").__dict__),
                    )

connection.close()

