import pika, os
from dotenv import load_dotenv

class Config:
    def __init__(self):
        load_dotenv()

        self.RABBITMQ_USER = os.getenv('RABBITMQ_USER', 'admin')
        self.RABBITMQ_PASS = os.getenv('RABBITMQ_PASS', 'admin')
        self.RABBITMQ_HOST = os.getenv('RABBITMQ_HOST', 'localhost')
        self.RABBITMQ_PORT = os.getenv('RABBITMQ_PORT', 5672)
        self.RABBITMQ_QUEUE = os.getenv('RABBITMQ_QUEUE', 'results')


    def generate_Pika_ConnectionParameters(self) -> pika.ConnectionParameters:
        return pika.ConnectionParameters(
            self.RABBITMQ_HOST,
            self.RABBITMQ_PORT,
            credentials=pika.PlainCredentials(self.RABBITMQ_USER, self.RABBITMQ_PASS)
        )
