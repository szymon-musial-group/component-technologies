from dataclasses import dataclass
from datetime import datetime

@dataclass
class SampleDataClass:
    message: str
    time: str = datetime.now().isoformat() # isoformat
        
class SampleClass(SampleDataClass):
    def __init__(self, message, time = datetime.now() ):
        SampleDataClass.__init__(self, message, time) 

    def print(self):
        print(f'Msg: {self.message} at {datetime.fromisoformat(self.time).strftime("%X")}')
        