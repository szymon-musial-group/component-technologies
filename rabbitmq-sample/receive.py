#!/usr/bin/env python
import pika, sys, os, json, pickle
from config import Config
from sample_class import SampleClass

def try_to_parse_as_json(channel, method, properties, body) -> bool:
    try:
        raw_obj = json.loads(body)
        obj = SampleClass(raw_obj['message'], raw_obj['time'])
        obj.print()
        channel.basic_ack(method.delivery_tag)
        return True
    except:
        return False

def try_to_parse_as_pickle(channel, method, properties, body) -> bool:
    try:          
        pickle.loads(body).print()
        channel.basic_ack(method.delivery_tag)
        return True
    except:
        return False

def try_to_parse_as_string(channel, method, properties, body) -> bool:
    try:            
        string = str(body, 'UTF-8')
        print(string)
        channel.basic_ack(method.delivery_tag)
        return True
    except:
        return False

def process_incoming_message(channel, method, properties, body):
    print(f"-----{method.delivery_tag}-----")
    print(f" [channel] {channel}")
    print(f" [method] {method}")
    print(f" [properties ] {properties}")
    print(f" [body ] {body}")
    
    if try_to_parse_as_json(channel, method, properties, body):
        return
    
    if try_to_parse_as_pickle(channel, method, properties, body):
        return
    
    if try_to_parse_as_string(channel, method, properties, body):
        return
   
    channel.basic_reject(method.delivery_tag)
    print(f" Rejected {body}")

def main():
    config = Config()

    connection = pika.BlockingConnection(config.generate_Pika_ConnectionParameters())
    channel = connection.channel()
    
    # Uncomment if queue durable = false to dynamically create it - note that it's not persistent after py app exit
    # channel.queue_declare(queue='hello')  

    channel.basic_consume(queue=config.RABBITMQ_QUEUE, on_message_callback=process_incoming_message, 
                          # auto_ack=True # life on the edge
                          )

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
