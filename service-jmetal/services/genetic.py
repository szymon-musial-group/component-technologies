from core.problems.tsp import tsp
from core.algorithms.genetic import genetic_run
from jmetal.operator.crossover import PMXCrossover
from jmetal.operator.mutation import PermutationSwapMutation
from core.problems.tsp import tsp


class GeneticAlgorithmService:
    def call(
        self,
        problem_type,
        graph,
        mutation_type="PermutationSwapMutation",
        crossover_type="PMXCrossover",
        population=[],
        iterations=1000,
        population_size=100,
        fitness_registrator=None,
    ):
        problem_instance = None
        crossover = None
        mutation = None

        if mutation_type == "PermutationSwapMutation" or not mutation_type:
            mutation = PermutationSwapMutation(0.5)
        else:
            return "Not supported mutation type"

        if crossover_type == "PMXCrossover" or not crossover_type:
            crossover = PMXCrossover(0.8)
        else:
            return "Not supported mutation type"

        if problem_type == "tsp":
            problem_instance = tsp(graph)
        else:
            return "Problem not supported"

        return genetic_run(
            problem_instance,
            mutation,
            crossover,
            evaluations=iterations,
            population=population,
            population_size=population_size,
            fitness_registrator=fitness_registrator,
        )
