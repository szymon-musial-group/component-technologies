from services.genetic import GeneticAlgorithmService
from ctcommons.rabbitmq.worker_controller import WorkerNodeController


class JMetalWorkerController(WorkerNodeController):
    def __init__(self):
        self.service = GeneticAlgorithmService()
        super().__init__(self._handle_job)

    def _handle_job(self, parsed, fitness_registrator=None):
        problem_type = parsed["problem"]["type"]
        population_size = (
            parsed.get("algorithm", {})
            .get("initialization", {})
            .get("initialPopulation", None)
        )
        graph = parsed["problem"]["parameters"]["distanceMatrix"]
        crossover_type = (
            parsed.get("algorithm", {}).get("crossover", {}).get("method", None)
        )
        mutation_type = (
            parsed.get("algorithm", {}).get("mutation", {}).get("method", None)
        )
        population = parsed.get("population", [])
        iterations = (
            parsed.get("algorithm", {}).get("stopCondition", {}).get("ngen", None)
        )

        return self.service.call(
            problem_type,
            graph,
            mutation_type,
            crossover_type,
            population=population,
            iterations=iterations,
            population_size=population_size,
            fitness_registrator=fitness_registrator,
        )
