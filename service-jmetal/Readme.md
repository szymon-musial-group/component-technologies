# Service-DEAP

This is a Flask application which uses DEAP python package for evaluation of evolutionary algorithms.

## Requirements

- Python 3.10 or higher
- Poetry
- Docker

## Setup

1. Run `poetry install` to fetch the dependencies
2. Run `poetry run pre-commit install` to setup pre-commit hooks

## Development

1. Run `poetry shell` to enter poetry shell
2. Execute  
   `export FLASK_APP=api/main.py && flask run`  
   to start up the service
3. Execute request in `rest` directory (using e.g. vscode rest-client extension)

## Testing

1. Outside of poetry shell: `poetry run pytest`
2. Within poetry shell: `pytest`

## Docker

1. Build image with following command:  
   `docker build -t service-deap .`
2. Run image with following command:  
   `docker run --name service-deap -p 5000:5000 service-deap`
3. Remove container using commands:  
   `docker stop service-deap`  
   `docker rm service-deap`

4. If you want to start container which already exists, just run  
   `docker start service-deap`
