from jmetal.core.solution import PermutationSolution


class GeneticSerializer:
    @staticmethod
    def serializeSolutions(solutions: list[PermutationSolution]):
        return [solution.variables for solution in solutions]

    @staticmethod
    def deserializeSolutions(solutions: list[int]):
        deserializedSolutions = []
        for solution in solutions:
            deserializedSolution = PermutationSolution(len(solution), 1)
            deserializedSolution.variables = solution[:]
            deserializedSolutions.append(deserializedSolution)
        return deserializedSolutions
