import time

from core.algorithms.serializer import GeneticSerializer
from jmetal.algorithm.singleobjective.genetic_algorithm import GeneticAlgorithm
from jmetal.util.observer import Observer
from jmetal.util.termination_criterion import StoppingByEvaluations


class FitnessHistory(Observer):
    def __init__(self):
        self.fitness_history = []

    def update(self, *args, **kwargs):
        population = kwargs["SOLUTIONS"]
        self.fitness_history.append(population.objectives[0])

        ## currently`kwargs['SOLUTIONS']` returns `Solution` object, not a list of objects
        # for solution in population:
        #     self.fitness_history.append(solution.objectives[0])

    def get_history(self):
        return self.fitness_history


def genetic_run(
    problem,
    mutation,
    crossover,
    evaluations=1000,
    population_size=100,
    population=[],
    fitness_registrator=None,
):
    algorithm = GeneticAlgorithm(
        problem=problem,
        population_size=population_size,
        offspring_population_size=population_size,
        mutation=mutation,
        crossover=crossover,
        termination_criterion=StoppingByEvaluations(
            max_evaluations=evaluations + population_size
        ),
    )

    fitness_observer = FitnessHistory()
    algorithm.observable.register(fitness_observer)

    algorithm.start_computing_time = time.time()

    if population:
        algorithm.solutions = GeneticSerializer.deserializeSolutions(population)
    else:
        algorithm.solutions = algorithm.create_initial_solutions()

    algorithm.solutions = algorithm.evaluate(algorithm.solutions)
    algorithm.init_progress()

    algorithm.evaluations = 0

    for _ in range(evaluations):
        algorithm.step()
        algorithm.update_progress()

    algorithm.total_computing_time = time.time() - algorithm.start_computing_time
    result = algorithm.get_result()

    ## returns fitness scores from each iteration
    fitness_history = fitness_observer.get_history()
    print(f"fitness_history: {fitness_history}")

    if fitness_registrator:
        fitness_registrator.register(fitness_history[0], firstRegister=True)
        for fitness in fitness_history[1:]:
            fitness_registrator.register(fitness)

    return {
        "algorithm": algorithm.get_name(),
        "best_result": result.variables,
        "best_score": result.objectives[0],
        "solutions": GeneticSerializer.serializeSolutions(algorithm.solutions),
        "computingTime": algorithm.total_computing_time,
    }
