from jmetal.problem.singleobjective.tsp import TSP


def tsp(graph):
    problem = TSP(instance=f"resources/kroA100.tsp")
    problem.distance_matrix = graph
    problem.number_of_cities = len(graph)
    return problem
