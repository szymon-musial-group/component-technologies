from functools import partial

class TspSerializer:
    @staticmethod
    def serialize_individual(individual) -> list[int]:
        return list(individual)

    @staticmethod
    def deserialize_individual(individual_container, individual: list[int]):
        return individual_container(individual)

    @staticmethod
    def serialize_population(population: list) -> list[list[int]]:
        return list(map(TspSerializer.serialize_individual, population))

    @staticmethod
    def deserialize_population(population: list[list[int]], individual_container) -> list:
        return list(map(partial(TspSerializer.deserialize_individual, individual_container), population))
