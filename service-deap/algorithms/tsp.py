import array
import random

import numpy
from deap import algorithms, base, creator, tools

from .serializer import TspSerializer


def tsp(config: dict, fitness_registrator=None):
    problem_parameters = config["problem"]["parameters"]
    distance_map = problem_parameters["distanceMatrix"]
    ind_size = problem_parameters["tourSize"]
    algorithm_parameters = config["algorithm"]
    population_size = algorithm_parameters.get("initialization").get(
        "initialPopulation", 100
    )
    evaluations = algorithm_parameters.get("stopCondition").get("ngen", 40)
    population = config.get("population", [])

    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", array.array, typecode="i", fitness=creator.FitnessMin)

    toolbox = base.Toolbox()

    # Initialization
    toolbox.register("indices", random.sample, range(ind_size), ind_size)
    toolbox.register(
        "individual", tools.initIterate, creator.Individual, toolbox.indices
    )
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    def evalTSP(individual):
        distance = distance_map[individual[-1]][individual[0]]
        for gene1, gene2 in zip(individual[0:-1], individual[1:]):
            distance += distance_map[gene1][gene2]
        return (distance,)

    toolbox.register("mate", tools.cxPartialyMatched)
    toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.05)
    toolbox.register("select", tools.selTournament, tournsize=3)
    toolbox.register("evaluate", evalTSP)

    random.seed()

    pop = toolbox.population(n=population_size)
    if population:
        # if user passed population, use this instead - ideally we should pass already
        # prepared population to tsp() function, so that this if statement wouldn't be necessary
        pop = TspSerializer.deserialize_population(population, creator.Individual)

    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    _, logbook = algorithms.eaSimple(
        pop, toolbox, 0.7, 0.2, ngen=evaluations, stats=stats, halloffame=hof
    )

    fitness_history = logbook.select("min")
    print(f"fitness_history : {fitness_history}")
    if fitness_registrator:
        fitness_registrator.register(fitness_history[0], firstRegister=True)
        for fitness in fitness_history[1:]:
            fitness_registrator.register(fitness)

    best_individual = hof[0]

    return {
        "algorithm": "tsp",
        "best_result": TspSerializer.serialize_individual(best_individual),
        "best_score": best_individual.fitness.values[0],
        "solutions": TspSerializer.serialize_population(pop),
        "computingTime": -1,
    }
