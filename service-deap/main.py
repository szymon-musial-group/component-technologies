from ctcommons.rabbitmq.config import RabbitMQConfig
from ctcommons.rabbitmq.client import RabbitMQClient
from controllers.rabbitMQ import DeapWorkerController

config = RabbitMQConfig()
client = RabbitMQClient(config.generate_Pika_ConnectionParameters())
channel = client.open_channel()
message_controller = DeapWorkerController()
channel.basic_consume(
    queue="deap", on_message_callback=message_controller.handle_message
)

print(" [*] Waiting for messages. To exit press CTRL+C")
channel.start_consuming()
