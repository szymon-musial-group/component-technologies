from services.genetic import GeneticAlgorithmService
from ctcommons.rabbitmq.worker_controller import WorkerNodeController


class DeapWorkerController(WorkerNodeController):
    def __init__(self):
        self.service = GeneticAlgorithmService()

        super().__init__(self.service.call)
