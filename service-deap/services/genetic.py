from algorithms.tsp import tsp


class GeneticAlgorithmService:
    def call(self, config, fitness_registrator=None):
        if config["problem"]["type"] == "tsp":
            return tsp(config, fitness_registrator=fitness_registrator)
        else:
            return "Problem not supported"
